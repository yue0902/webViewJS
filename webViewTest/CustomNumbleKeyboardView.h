//
//  CustomNumbleKeyboardView.h
//  CustomNumbleKeyboard
//
//  Created by wi on 12-3-19.
//  Copyright (c) 2012年 Wi.Inc. All rights reserved.
//

/*
 自定义一个4行3列的键盘
 */

#import <UIKit/UIKit.h>

@protocol CustomNumberKeyBoardDelegate <NSObject>

//响应数字输入
- (void) numberKeyBoardInput:(NSInteger) number;
//响应空格输入
- (void) numberKeyBoardBackspace;
//响应完成按键输入
- (void) numberKeyBoardFinish;

@end

@interface CustomNumbleKeyboardView : UIWindow

@property(nonatomic, assign) id<CustomNumberKeyBoardDelegate> cdelegate;

-(CGFloat)getKeyboardRealHeight;
-(void)showCustomKeyboard;
-(void)dismissCustomKeyboard;

@end
